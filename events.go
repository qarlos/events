/*
TODO:
 - EventManager.RemoveListener
*/

package events

import (
	"reflect"
)

// Event
type Event interface {
	IsEvent()
}

/*
var RegisteredEventTypes []reflect.Type
func RegisterEventType(eventType Event) {
	RegisteredEventTypes = append(RegisteredEventTypes, reflect.TypeOf(eventType))
}
*/

// Event listener
type EventListener interface {
	HandleEvent(e Event)
}

// Event manager
type EventManager struct {
	listeners map[reflect.Type]([]EventListener)
}

func NewEventManager() *EventManager {
	return &EventManager{
		listeners: make(map[reflect.Type]([]EventListener)),
	}
}

func (manager *EventManager) AddListener(listener EventListener, eventTypes ...Event) {
	// Add the listener for all the event types specified
	for _, e := range eventTypes {
		T := reflect.TypeOf(e)
		manager.listeners[T] = append(manager.listeners[T], listener)
	}
}

func (manager *EventManager) TriggerEvent(e Event) {
	// get event type
	T := reflect.TypeOf(e)

	// handle the event only on listeners registered for this event type
	for _, listener := range manager.listeners[T] {
		listener.HandleEvent(e)
	}
}
