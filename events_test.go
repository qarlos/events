package events

import (
	"fmt"
	"strings"
	"testing"
)

// Event with some info
type NewsEvent struct {
	Info string
}

func (e *NewsEvent) IsEvent() {}

// Event triggered when two objects collide
type CollisionEvent struct {
	Obj1, Obj2 *Obj
}

func (e *CollisionEvent) IsEvent() {}

// Object (EventListener), stores the info received in a data field
type Obj struct {
	name string
	data string
}

func (o *Obj) HandleEvent(e Event) {
	switch event := e.(type) {
	case *NewsEvent:
		o.data = strings.Join([]string{o.data, event.Info}, ", ")
		fmt.Printf("Objeto '%v' recibe evento %v...", o.name, "NewsEvent")
		fmt.Printf("Info: %v\n", event.Info)
	case *CollisionEvent:
		fmt.Printf("Objeto '%v' recibe evento %v...", o.name, "CollisionEvent")
		fmt.Printf("Obj1: %v, Obj2: %v\n", event.Obj1.name, event.Obj2.name)
	default:
		fmt.Printf("Objeto '%v' recibe otro evento (%T)\n", o.name, e)
	}
}

// TEST
func TestEvents(t *testing.T) {
	manager := NewEventManager()

	Player := Obj{"Player", ""}
	Item := Obj{"Item", ""}

	fmt.Printf("Objeto '%v' escuchará eventos CollisionEvent y NewsEvent\n", Player.name)
	manager.AddListener(&Player, (*CollisionEvent)(nil), (*NewsEvent)(nil))
	fmt.Printf("Objeto '%v' escuchará eventos NewsEvent\n", Item.name)
	manager.AddListener(&Item, (*NewsEvent)(nil))

	fmt.Printf("Emitir evento NewsEvent con info 'Hola'...\n")
	manager.TriggerEvent(&NewsEvent{"Hola"})
	fmt.Printf("Emitir evento NewsEvent con info 'Adiós'...\n")
	manager.TriggerEvent(&NewsEvent{"Adiós"})

	fmt.Printf("Emitir evento CollisionEvent con objetos Player e Item...\n")
	manager.TriggerEvent(&CollisionEvent{&Player, &Item})

	fmt.Println("Done")
}

/*
func init() {
	RegisterEventType((*NewsEvent)(nil))
	RegisterEventType((*CollisionEvent)(nil))
}
*/
